﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using WBTest.Controller;
using WBTest.Model;
using WBTest.Parameters;

namespace WBTest.View
{

    public class PlayerInfoView : MonoBehaviour
    {

#pragma warning disable
        [SerializeField]
        Text txtName;
        [SerializeField]
        Image imgEnergyLine;
#pragma warning restore

        ParametersBinding binding;
        CharacterParameters character;

        float energy, energyMax, recoverySpeed;

        Coroutine recoverRoutine;

        void Start()
        {
            binding = GetComponentInParent<ParametersBinding>();
            character = binding.Parameters[Constants.CHARACTER_PARAMS] as CharacterParameters;

            energy = character.Energy;
            energyMax = character.EnergyMax;
            recoverySpeed = character.EnergyRecovery;

            txtName.text = character.Name;

            character.OnEnergyRecoveryStart += OnEnergyRecoveryStart;
            character.OnEnergyRecoveryStop += OnEnergyRecoveryFinish;
            character.OnEnergyConsume += OnEnergyConsume;
        }

        void OnDestroy()
        {
            character.OnEnergyRecoveryStart -= OnEnergyRecoveryStart;
            character.OnEnergyRecoveryStop -= OnEnergyRecoveryFinish;
            character.OnEnergyConsume -= OnEnergyConsume;
        }

        void OnEnergyRecoveryStart(int newEnergy)
        {
            if(recoverRoutine!=null)
                StopCoroutine(recoverRoutine);
            energy = newEnergy;
            recoverRoutine = StartCoroutine(RecoveryEnergyLine());
        }

        void OnEnergyRecoveryFinish(int newEnergy)
        {
            if (recoverRoutine != null)
                StopCoroutine(recoverRoutine);
            energy = newEnergy;
            imgEnergyLine.fillAmount = energy / energyMax;
        }

        void OnEnergyConsume(int newEnergy)
        {
            energy = newEnergy;
            imgEnergyLine.fillAmount = energy / energyMax;
        }

        IEnumerator RecoveryEnergyLine()
        {
            while (energy < energyMax)
            {
                energy += recoverySpeed * Time.deltaTime;
                imgEnergyLine.fillAmount = energy / energyMax;
                yield return null;
            }
        }

    }
}