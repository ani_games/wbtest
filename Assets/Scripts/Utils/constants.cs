﻿namespace WBTest
{

    public static class Constants
    {
        //Parameters
        public const string ACTION_STATE = "ActionState";
        public const string CHARACTER_PARAMS = "CharacterParams";

        //Objects
        public const string MAIN_CAMERA_OBJECT = "MainCameraObject";
        public const string MAIN_PLAYER_OBJECT = "MainPlayerObject";
        public const string UI_CONTROL_OBJECT = "UiControlObject";
        
    }


}