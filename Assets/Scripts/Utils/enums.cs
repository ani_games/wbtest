﻿namespace WBTest
{

    /// <summary>
    /// Общий набор стейтов действий для всех игровых сущностей
    /// </summary>
    public enum ActionState
    {
        Idle,
        Attack,
        Healing,
        Fall,
        Dance,
        Praying,
        Recovery

    }

}
