﻿namespace WBTest
{
    public delegate void StartActionHandler(ActionState state);
    public delegate void FinishActionHandler(ActionState state);
}