﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WBTest.Controller;

namespace WBTest.Parameters
{

    public class CharacterParameters : BaseParameters
    {
        public event System.Action<int> OnEnergyRecoveryStart, OnEnergyRecoveryStop, OnEnergyConsume;

        //Permanent
#pragma warning disable
        [SerializeField]
        string gameId, name;
        [SerializeField]
        int energyMax, enrgRecoverySpeed;
#pragma warning restore

        //Current
        int energy=100;

        //Access
        public string GameId { get { return gameId; } }
        public string Name { get { return name; } }
        public int EnergyMax { get { return energyMax; } }
        public int EnergyRecovery { get { return enrgRecoverySpeed; } }
        public int Energy
        { get
            {/*
                if(EnergySinceSyncroTime>0)
                    SyncronizeEnergy();*/
                return energy;
            }
            set
            {
                energy = value;
                if (OnEnergyConsume != null)
                    OnEnergyConsume(energy);
            }
        }

        IStateObservable stateOwner;
        StateInfoParameters stateParameters;

        public float EnergySinceSyncroTime { get; set; }
        

        //Methods
        public void Initialize()
        {
            energy = energyMax;
            EnergySinceSyncroTime = Time.time; 
        }

        
        public  void SyncronizeEnergy()
        {
            if (EnergySinceSyncroTime == 0)
                return;

            int ticks = (int)(Time.time - EnergySinceSyncroTime);
            EnergySinceSyncroTime = Time.time - ((Time.time - EnergySinceSyncroTime) - ticks);//оставим милисекунды
            //На случай если мы будем синхронизироваться без перехода из стейта в стейт

            energy = Mathf.Clamp(energy + ticks * enrgRecoverySpeed, 0, EnergyMax);
        }

        public void StartEnergyRecovery()
        {
            if (OnEnergyRecoveryStart != null)
                OnEnergyRecoveryStart(energy);
        }

        public void StopEnergyRecovery()
        {
            if (OnEnergyRecoveryStop != null)
                OnEnergyRecoveryStop(energy);
        }
    
    }
}