﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WBTest.Parameters
{

    public class BaseParameters : ScriptableObject
    {
        [SerializeField]
        string id;

        public string Id { get { return id; } }

    }
}