﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WBTest
{

    /// <summary>
    /// Содержит в себе данные о анимационных стейтах.
    /// </summary>
    [System.Serializable]
    public class StateInfo
    {
        [SerializeField]
        ActionState state;
        [SerializeField]
        float duration;
        [SerializeField]
        string trigger;
        [SerializeField]
        int energy;

        //Создается только сериализацией
        private StateInfo()
        {}

        //Интерфейс взаимоействия
        public ActionState State { get { return state; } }
        public float Duration { get { return duration; } }
        public string Trigger { get { return trigger; } }
        public int Energy { get { return energy; } }
    }
}