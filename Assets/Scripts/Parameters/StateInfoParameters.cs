﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WBTest.Parameters
{

    /// <summary>
    /// Объект настроек анимаций для игровой сущности
    /// </summary>

    public class StateInfoParameters : BaseParameters
    {

        [SerializeField]
        StateInfo[] stateInfo;

        Dictionary<ActionState, StateInfo> stateInfoDictionary = null;

        public StateInfo GetStateInfo(ActionState state)
        {

            if (stateInfoDictionary == null)
            {
                stateInfoDictionary = new Dictionary<ActionState, StateInfo>();
                for (int i = 0; i < stateInfo.Length; i++)
                {
                    stateInfoDictionary.Add(stateInfo[i].State, stateInfo[i]);
                }
            }

            return stateInfoDictionary[state];

        }


    }
}