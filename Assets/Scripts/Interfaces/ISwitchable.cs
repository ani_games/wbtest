﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WBTest
{

    public interface ISwitchable
    {
        event System.Action OnSwitchOn, OnSwitchOff; 
    }
}