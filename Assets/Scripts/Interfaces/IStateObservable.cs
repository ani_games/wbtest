﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WBTest
{

    public interface IStateObservable
    {
        void OnStartStateSubscribe(StartActionHandler handler);
        void OnStartStateUnsubscribe(StartActionHandler handler);
        void OnFinishStateSubscribe(FinishActionHandler handler);
        void OnFinishStateUnsubscribe(FinishActionHandler handler);
    }
}