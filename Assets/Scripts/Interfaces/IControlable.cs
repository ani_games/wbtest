﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WBTest;

/// <summary>
/// Интерфейс для реализации классов Инпута
/// </summary>
public interface IControlable
{
    event System.Action<ActionState> OnStateChanged;
}
