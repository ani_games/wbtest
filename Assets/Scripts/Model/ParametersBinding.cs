﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WBTest.Parameters;

namespace WBTest.Model
{

    public class ParametersBinding : MonoBehaviour
    {
        [SerializeField]
        BaseParameters[] parameters;

        public Dictionary<string, BaseParameters> Parameters;

        void Awake()
        {
            Parameters = new Dictionary<string, BaseParameters>();
            for (int i = 0; i < parameters.Length; i++)
            {
                Parameters.Add(parameters[i].Id, parameters[i]);
            }
        }

    }
}