﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WBTest.Model;
using WBTest.Parameters;

namespace WBTest.Controller
{
    [RequireComponent(typeof(ParametersBinding))]
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(IStateObservable))]
    public class AnimationController : MonoBehaviour
    {

        IStateObservable stateOwner;
        StateInfoParameters stateInfoParams;
        Animator animator;

        void Start()
        {
            animator = GetComponent<Animator>();
            stateOwner = GetComponent<IStateObservable>();

            stateInfoParams = GetComponent<ParametersBinding>().Parameters[Constants.ACTION_STATE] as StateInfoParameters;
            if (stateInfoParams == null)
                Debug.LogError("There is no State Info parameters in current Params Binding");

            stateOwner.OnStartStateSubscribe(OnActionStart);
            stateOwner.OnFinishStateSubscribe(OnActionFinish);
        }

        
        void OnDestroy()
        {
            stateOwner.OnStartStateUnsubscribe(OnActionStart);
            stateOwner.OnFinishStateUnsubscribe(OnActionFinish);
        }

        void OnActionStart(ActionState state)
        {
            animator.SetTrigger(stateInfoParams.GetStateInfo(state).Trigger);
        }

        void OnActionFinish(ActionState state)
        {

        }
    }
}