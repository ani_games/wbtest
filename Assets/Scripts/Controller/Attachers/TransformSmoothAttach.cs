﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformSmoothAttach : MonoBehaviour
{

    [SerializeField]
    Transform attachTarget;

    [SerializeField]
    Vector3 offsetVector;

    [SerializeField]
    float speedFactor;


	void Update ()
    {
        transform.position = Vector3.Lerp(transform.position, attachTarget.position + offsetVector, Time.deltaTime * speedFactor);
    }
}
