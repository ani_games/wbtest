﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WBTest.Controller
{

    public class PlayerInfoAttach : MonoBehaviour
    {
#pragma warning disable
        [SerializeField]
        Transform attachTarget;

        [SerializeField]
        Vector3 offsetVector;
#pragma warning restore

        //Камеру не привязываем жестко, чтобы не иметь связей у префаба на объекты вне префаба
        Transform lookTarget;

        void Start()
        {
            lookTarget = DepencyManager.Instance.GetGameObjectByID(Constants.MAIN_CAMERA_OBJECT).transform;
        }

        void Update()
        {
            transform.position = Vector3.Lerp(
                transform.position, attachTarget.position + 
                offsetVector, Time.deltaTime * 10f);
            transform.LookAt(lookTarget);
        }
    }
}