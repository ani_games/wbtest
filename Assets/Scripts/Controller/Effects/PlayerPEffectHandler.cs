﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WBTest.Controller
{

    public class PlayerPEffectHandler : MonoBehaviour,ISwitchable
    {
        public event Action OnSwitchOn;
        public event Action OnSwitchOff;

        IStateObservable observable;
        
        void Start()
        {
            observable = GetComponent<IStateObservable>();
            if (observable != null)
            {
                observable.OnStartStateSubscribe(OnActionStart);
                observable.OnFinishStateSubscribe(OnActionFinish);
            }
            else
            {
                Debug.LogError("Object hasn't this Type");
            }

        }

        private void OnDestroy()
        {
            if (observable != null)
            {
                observable.OnStartStateUnsubscribe(OnActionStart);
                observable.OnFinishStateUnsubscribe(OnActionFinish);
            }
        }

        void OnActionStart(ActionState state)
        {
            if (OnSwitchOn != null)
                OnSwitchOn();
        }

        void OnActionFinish(ActionState state)
        {
            if (OnSwitchOff != null)
                OnSwitchOff();
        }
    }
}