﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WBTest.Controller
{
    /// <summary>
    /// Класс отвечает за непосредственное включение/отключение постеффекта
    /// </summary>
    public class PostEffectController : MonoBehaviour
    {

        [SerializeField]
        MonoBehaviour postEffect;

        /// <summary>
        /// Объект, который определяет включение/выключение постэффекта.
        /// </summary>
        ISwitchable effectOwner;

        void Start()
        {
            effectOwner = DepencyManager.Instance.postEffectHandler;
            effectOwner.OnSwitchOn += EffectOwner_OnPostEffectOn;
            effectOwner.OnSwitchOff += EffectOwner_OnPostEffectOff;
        }

        void OnDestroy()
        {
            effectOwner.OnSwitchOn -= EffectOwner_OnPostEffectOn;
            effectOwner.OnSwitchOff -= EffectOwner_OnPostEffectOff;
        }

        private void EffectOwner_OnPostEffectOn()
        {
            postEffect.enabled = true;
        }

        private void EffectOwner_OnPostEffectOff()
        {
            postEffect.enabled = false;
        }      

    }
}