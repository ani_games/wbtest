﻿using System.Runtime.InteropServices;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;

namespace WBTest.Controller
{
    /// <summary>
    /// Аура должна быть дочерним объектом Владельца Ауры (Иметь компонент, реализующий IStateObservable)
    /// </summary>
    public class AuraEffect : MonoBehaviour
    {

#pragma warning disable
        [SerializeField]
        MeshRenderer aura;
        [SerializeField]
        float speed = 1;
        [SerializeField]
        [Range(0f, 1f)]
        float cut_off;
        [SerializeField]
        [Range(20,100)]
        int updateMillisecs = 30;
#pragma warning restore

        IStateObservable owner;

        Texture mainTexTemplate;
        Texture maskTexTemplate;

        System.IntPtr mainTexPtr;
        System.IntPtr maskTexPtr;        

        Texture2D mainTex, maskTex, noMask;

        //Thread management
        Thread texUpd;
        float time = 0;
        float lastUpdTime = 0;
        bool needUpdate = false;

        void Start()
        {
            owner = transform.GetComponentInParent<IStateObservable>();
            if (owner != null)
            {
                owner.OnStartStateSubscribe(OnOwnerActionStart);
                owner.OnFinishStateSubscribe(OnOwnerActionFinish);
            }
            else
            {
                Debug.LogError("Player doesn't have such Type");
            }
            
            mainTexTemplate = new Texture2D(512, 512, TextureFormat.BGRA32, false, false);
            maskTexTemplate = new Texture2D(512, 512, TextureFormat.BGRA32, false, false);

            maskTexPtr = maskTexTemplate.GetNativeTexturePtr();
            mainTexPtr = mainTexTemplate.GetNativeTexturePtr();

            SetTexture(mainTexPtr, maskTexTemplate.width, maskTexTemplate.height);
            CreateMaskTexture(maskTexPtr, mainTexTemplate.width, mainTexTemplate.height,(int)(cut_off*255));

            mainTex = Texture2D.CreateExternalTexture(maskTexTemplate.width, maskTexTemplate.height, TextureFormat.BGRA32, false, false, mainTexPtr);
            maskTex = Texture2D.CreateExternalTexture(maskTexTemplate.width, maskTexTemplate.height, TextureFormat.BGRA32, false, false, maskTexPtr);
            noMask = new Texture2D(512, 512, TextureFormat.BGRA32, false, false);

            aura.material.mainTexture = mainTex;
            aura.material.SetTexture("_Mask", noMask);

            StartTexUpdateThread();
        }

        private void OnDestroy()
        {
            
            if (owner != null)
            {
                owner.OnStartStateUnsubscribe(OnOwnerActionStart);
                owner.OnFinishStateUnsubscribe(OnOwnerActionFinish);
            }
            InterruptTexUpdateThread();
        }

        void Update()
        {
            time = Time.time;
        }

        #region Update Mask Thread

        void StartTexUpdateThread()
        {
            needUpdate = true;
            texUpd = new Thread(UpdateTexture);
            texUpd.Start();
            time = lastUpdTime = Time.time;
        }

        void StopTexUpdateThread()
        {
            needUpdate = false;            
        }

        void InterruptTexUpdateThread()
        {
            texUpd.Interrupt();
        }

        void UpdateTexture()
        {
            int sleepTime = 0;

            while (needUpdate)
            {
                UpdateTexture(time * speed);
                sleepTime = Mathf.Clamp(updateMillisecs - (int)((time - lastUpdTime) * 1000), 0, updateMillisecs);//Выравним 
                Thread.Sleep(sleepTime);
                lastUpdTime = time;
            }
        }
        #endregion

        #region External

        [DllImport("AuraGenerator")]
        public static extern int Ping();

        [DllImport("AuraGenerator")]
        public static extern void SetTexture(System.IntPtr texture, int width, int height);

        [DllImport("AuraGenerator")]
        public static extern void CreateMaskTexture(System.IntPtr tex_ptr, int t_wid, int t_heit, int cutoff);

        [DllImport("AuraGenerator")]
        public static extern void UpdateTexture(float time);

        #endregion

        #region player state change

        void OnOwnerActionStart(ActionState state)
        {
            if (state == ActionState.Fall)
            {
                aura.enabled = false;
            }
            else
            {
                if (!aura.enabled)
                    aura.enabled = true;
                aura.material.SetTexture("_Mask", maskTex);
            }
        }

        void OnOwnerActionFinish(ActionState state)
        {
            aura.enabled = true;
            aura.material.SetTexture("_Mask", noMask);
        }

        #endregion

    }
}