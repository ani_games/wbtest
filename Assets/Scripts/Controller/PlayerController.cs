﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WBTest.Parameters;
using WBTest.Model;

namespace WBTest.Controller
{ 

    [RequireComponent(typeof(ParametersBinding))]
    public class PlayerController : MonoBehaviour, IStateObservable
    {

        IControlable controller;
        ParametersBinding binding;

        StateInfoParameters stateInfoParams;
        CharacterParameters charParams;

        StartActionHandler startActionHandler;
        FinishActionHandler finishActionHandler;

        #region MonoBehavior

        void Start()
        {
            binding = GetComponent<ParametersBinding>();
            controller = DepencyManager.Instance.inputControl;
            stateInfoParams = binding.Parameters[Constants.ACTION_STATE] as StateInfoParameters;
            charParams = binding.Parameters[Constants.CHARACTER_PARAMS] as CharacterParameters;
            controller.OnStateChanged += ChangeState;

            charParams.Initialize();
        }

        private void OnDestroy()
        {
            controller.OnStateChanged -= ChangeState;
        }
        #endregion

        #region Change State Handling
        void ChangeState(ActionState state)
        {
            StateInfo curStateInfo = stateInfoParams.GetStateInfo(state);

            if (curStateInfo == null)
            {
                Debug.LogError( String.Format("There is no Action State with state: {0} in current Param Binding", state));
            }

            if (charParams.Energy >= curStateInfo.Energy)
            {
                if (!curStateInfo.Trigger.Equals(string.Empty) && curStateInfo.Duration > 0)
                {
                    SwitchState(state, curStateInfo.Duration);
                }
            }
            else
            {
                SwitchState(ActionState.Recovery, stateInfoParams.GetStateInfo(ActionState.Recovery).Duration);
            }
            
        }

        void SwitchState(ActionState state, float duration)
        {
            StopAllCoroutines();
            StartCoroutine(WaitForActionEnd(state, duration));
            ApplyStateToEnergy(state);//Расходовать энергию на новое действие

            if (startActionHandler != null)
                startActionHandler.Invoke(state);
        }

        IEnumerator WaitForActionEnd(ActionState state, float duration)
        {
            yield return new WaitForSeconds(duration);

            if (finishActionHandler != null)
                finishActionHandler.Invoke(state);

            if (state == ActionState.Recovery)
                RecoveryEnergy();

            if (state == ActionState.Recovery && charParams.Energy < charParams.EnergyMax)
                ChangeState(ActionState.Recovery);
            else
                ChangeState(ActionState.Idle);

            
        }
        #endregion

        #region IStateObservable

        public void OnStartStateSubscribe(StartActionHandler handler)
        {
            startActionHandler += handler;
        }

        public void OnStartStateUnsubscribe(StartActionHandler handler)
        {
            startActionHandler -= handler;
        }

        public void OnFinishStateSubscribe(FinishActionHandler handler)
        {
            finishActionHandler += handler;
        }

        public void OnFinishStateUnsubscribe(FinishActionHandler handler)
        {
            finishActionHandler -= handler;
        }

        #endregion

        #region Energy Control
        //Восстановление энергии происходит небольшими этапами
        //Каждый раз выходя из этапа восстановления, мы синхронизируем энергию
        //Также при прерывании синхронизации 
        void ApplyStateToEnergy(ActionState state)
        {
            #region Energy Recovery
            if (state == ActionState.Recovery)
            {
                charParams.EnergySinceSyncroTime = Time.time;
                charParams.StartEnergyRecovery();                
            }
            else
            {
                //Кейс прерывания восстановления энергии или Выхода в Айдл при полном восстановлении
                if (charParams.EnergySinceSyncroTime != 0 || (state == ActionState.Idle && charParams.Energy == charParams.EnergyMax))
                {
                    charParams.SyncronizeEnergy();//Синхронизируем энергию
                    charParams.StopEnergyRecovery();
                }
                charParams.EnergySinceSyncroTime = 0;//Если время с последней синхронизации равно нулю, синхронизация проводиться не будет

            }
            #endregion

            charParams.Energy -= stateInfoParams.GetStateInfo(state).Energy;
        }

        void RecoveryEnergy()
        {
            charParams.SyncronizeEnergy();
        }

        #endregion
    }
}