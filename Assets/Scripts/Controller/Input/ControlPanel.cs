﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace WBTest.Controller
{

    public class ControlPanel : MonoBehaviour, IControlable
    {
        public event Action<ActionState> OnStateChanged;

#pragma warning disable
        [SerializeField]
        Button btnAttack, btnHealing, btnFalling, btnDance, btnPraying;
#pragma warning restore

        void Start()
        {
            btnAttack.onClick.AddListener(() => OnStateChanged(ActionState.Attack));
            btnHealing.onClick.AddListener(() => OnStateChanged(ActionState.Healing));
            btnFalling.onClick.AddListener(() => OnStateChanged(ActionState.Fall));
            btnDance.onClick.AddListener(() => OnStateChanged(ActionState.Dance));
            btnPraying.onClick.AddListener(() => OnStateChanged(ActionState.Praying));
        }

        
        void OnDestroy()
        {
            btnAttack.onClick.RemoveAllListeners();
            btnHealing.onClick.RemoveAllListeners();
            btnFalling.onClick.RemoveAllListeners();
            btnDance.onClick.RemoveAllListeners();
            btnPraying.onClick.RemoveAllListeners();
        }
    }
}