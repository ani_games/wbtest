﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WBTest.Controller
{
    /// <summary>
    /// Точка доступа для большинства игровых сущностей таких, как
    /// Главный персонаж, контроллер вводы, обработчик событий включения/выключения Постэффекта и т.д.
    /// </summary>
    public class DepencyManager : MonoBehaviour
    {
        //Singleton
        public static DepencyManager Instance;

#pragma warning disable
        [SerializeField]
        GameObject player, rightControlPanel, cameraObject;
#pragma warning restore

        /// <summary>
        /// Данный словарь хранит пары ID - Ссылка на GameObject
        /// для прямого доступа.
        /// </summary>
        //Имеет смысл хранить только уникальные объекты такие, как главный (управляемый игроком) персонаж,
        //Инпут Контроллер, Объект Камеры.
        Dictionary<string, GameObject> idObjectDictionary;

        public ISwitchable postEffectHandler;

        public IControlable inputControl;

        private void Awake()
        {

            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                if (this != Instance)
                    DestroyImmediate(this.gameObject);
            }

            InitIdObjectDict();

            postEffectHandler = GetPlayerEntity<PlayerPEffectHandler>();
            inputControl = rightControlPanel.GetComponent<ControlPanel>();
        }

        public T GetPlayerEntity<T>()
        {
            return player.GetComponent<T>();
        }

        public GameObject GetGameObjectByID(string id)
        {
            return idObjectDictionary[id];
        }

        //Инициализация Ид-Объект словаря и добавление объектов
        void InitIdObjectDict()
        {
            idObjectDictionary = new Dictionary<string, GameObject>();
            idObjectDictionary.Add(Constants.MAIN_CAMERA_OBJECT, cameraObject);
            idObjectDictionary.Add(Constants.MAIN_PLAYER_OBJECT, player);
            idObjectDictionary.Add(Constants.UI_CONTROL_OBJECT, rightControlPanel);
        }

    }
}