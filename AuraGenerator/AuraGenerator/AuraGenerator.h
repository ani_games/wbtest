#pragma once

#define AURA_GENERATOR_API __declspec(dllexport) 
#define AURA_BACK __stdcall

extern "C"
{
	extern "C" int AURA_GENERATOR_API Ping();

	extern "C" void AURA_GENERATOR_API SetTexture(void* texture, int w, int h);

	extern "C" void AURA_GENERATOR_API UpdateTexture(float elapsedTime);

	extern "C" void AURA_GENERATOR_API CreateMaskTexture(void* tex_ptr, int t_wid, int t_heit, int cut_off);

	void CreateNoiseTexture();

	void EndModifyTexture(void* textureHandle, int textureWidth, int textureHeight, int rowPitch, void* dataPtr);

	void* BeginModifyTexture(void* textureHandle, int textureWidth, int textureHeight, int* outRowPitch);

	void CreateTexture();

}