#include "AuraGenerator.h"
#include <math.h>
#include "GLEW\glew.h"
#include "Unity\IUnityGraphics.h"
#include <d3d9.h>
#include "assert.h"

void* native_texture;
unsigned char* data;
int tex_width;
int tex_height;
int modify_texture;
//intptr_t imagePointer;

void* pixels;

extern "C" int Ping()
{
	//CreateTexture();
	return 1;
}

extern "C" void SetTexture(void* texture, int w, int h)
{
	//native_texture = texture;
	//native_texture[0] = 1;
	native_texture = texture;
	tex_width = w;
	tex_height = h;
	CreateNoiseTexture();
}


extern "C" void UpdateTexture(float time)
{
	void* textureHandle = native_texture;
	int width = tex_width;
	int height = tex_height;
	if (!textureHandle)
		return;

	int textureRowPitch;
	void* textureDataPtr = BeginModifyTexture(textureHandle, width, height, &textureRowPitch);
	if (!textureDataPtr)
		return;

	const float t = time * 4.0f;
	int i = 0;
	unsigned char* dst = (unsigned char*)textureDataPtr;
	for (int y = 0; y < height; ++y)
	{
		unsigned char* ptr = dst;
		for (int x = 0; x < width; ++x)
		{
			// Simple "plasma effect": several combined sine waves

			int vv = int(
				((sinf(30.0f*(float)x / (float)width)) * 40)+
				((sinf(t + 100.0f*(float)y / (float)width)) * 40)+
				((sinf(-t + 130.0f*(float)y / (float)width)) * 40) + 130
				);

			// Write the texture pixel
			/*
			ptr[0] = vv;
			ptr[1] = vv;
			ptr[2] = vv;
			ptr[3] = vv;*/

			// To next pixel (our pixels are 4 bpp)
			//ptr += 4;

			dst[i] =  255;
			dst[i + 1] = vv * 0.75f;
			dst[i + 2] = vv * 0.4f;
			dst[i + 3] = vv * 1.0f;
			i += 4;
			
		}

		// To next image row
		//dst += textureRowPitch;
	}

	EndModifyTexture(textureHandle, width, height, textureRowPitch, textureDataPtr);
}


void CreateNoiseTexture()
{

	void* textureHandle = native_texture;
	int width = tex_width;
	int height = tex_height;
	if (!textureHandle)
		return;

	int textureRowPitch;
	void* textureDataPtr = BeginModifyTexture(textureHandle, width, height, &textureRowPitch);
	if (!textureDataPtr)
		return;

	unsigned char* dst = (unsigned char*)textureDataPtr;

	int i = 0;

	for (int y = 0; y < height; ++y)
	{
		for (int x = 0; x < width; ++x)
		{
			int col = int(
				((sinf(100.0f*(float)x / (float)width) ) * 50 )*(height - y * 2.0f) / (float)height +
				((sinf(100.0f*(float)x / (float)width) ) * 50)*(y*2.0f) / (float)height
				);

			if (col < 0)col = 0;
			if (col > 255)col = 255;

			dst[i] = col;
			dst[i + 1] = col;
			dst[i + 2] = col;
			dst[i + 3] = col;
			i += 4;
		}

	}

	EndModifyTexture(textureHandle, width, height, textureRowPitch, textureDataPtr);

}



void* BeginModifyTexture(void* textureHandle, int textureWidth, int textureHeight, int* outRowPitch)
{
	IDirect3DTexture9* d3dtex = (IDirect3DTexture9*)textureHandle;
	assert(d3dtex);

	// Lock the texture and return pointer
	D3DLOCKED_RECT lr;
	HRESULT hr = d3dtex->LockRect(0, &lr, NULL, 0);
	if (FAILED(hr))
		return NULL;

	*outRowPitch = lr.Pitch;
	return lr.pBits;
}


void EndModifyTexture(void* textureHandle, int textureWidth, int textureHeight, int rowPitch, void* dataPtr)
{
	IDirect3DTexture9* d3dtex = (IDirect3DTexture9*)textureHandle;
	assert(d3dtex);

	// Unlock the texture after modification
	d3dtex->UnlockRect(0);
	//imagePointer = *native_texture;
}

void CreateTexture()
{
	
}


extern "C" void CreateMaskTexture(void* tex_ptr, int t_wid, int t_heit, int cut_off)
{

	void* textureHandle = tex_ptr;
	int width = t_wid;
	int height = t_heit;
	if (!textureHandle)
		return;

	int textureRowPitch;
	void* textureDataPtr = BeginModifyTexture(textureHandle, width, height, &textureRowPitch);
	if (!textureDataPtr)
		return;

	unsigned char* dst = (unsigned char*)textureDataPtr;

	int i = 0;
	for (int y = 0; y < height; ++y)
	{
		for (int x = 0; x < width; ++x)
		{
			int col = int(
				((sinf(100.0f*(float)x/(float)width)+1)*63+127)*(height-y)/(float)height
				);
			if (col < cut_off)
				col = 0;

			dst[i] = col;
			dst[i+1] = col;
			dst[i+2] = col;
			dst[i+3] = col;
			i+=4;
		}
		
	}

	EndModifyTexture(textureHandle, width, height, textureRowPitch, textureDataPtr);

}

